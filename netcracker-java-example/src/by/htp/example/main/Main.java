package by.htp.example.main;

import java.util.Date;

import by.htp.example.logic.Eating;
import by.htp.example.logic.EatingFactory;
import by.htp.example.logic.EatingParameters;

public class Main {

	public static void main(String[] args) {
		
		EatingFactory eatingFactory = new EatingFactory();
		
		Date date = new Date();
		
		Eating breakfast = eatingFactory.getEating(EatingParameters.BREAKFAST);
		breakfast.addEating(date);

		Eating lunch = eatingFactory.getEating(EatingParameters.LUNCH);
		lunch.addEating(date);
		
		Eating dinner = eatingFactory.getEating(EatingParameters.DINNER);
		dinner.addEating(date);
		
		

	}

}

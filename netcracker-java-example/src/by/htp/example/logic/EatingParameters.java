package by.htp.example.logic;

public final class EatingParameters {

	public EatingParameters() {}
	
	public static final String BREAKFAST = "Breakfast";
	public static final String LUNCH = "Lunch";
	public static final String DINNER = "Dinner";
	public static String EATING = "Eating";
}
